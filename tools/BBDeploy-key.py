from urllib import urlencode
from urllib2 import Request
from urllib2 import urlopen
from base64 import b64encode
from base64 import decode
import json

class RESTRequest(Request):
	def __init__(self, url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None):
		Request.__init__(self, url, data, headers, origin_req_host, unverifiable)
		self.method = method
		
	def get_method(self):
		if self.method:
			return self.method
		return Request.get_method(self)

class DeployKey():

	def __init__(self, spice_repo=None, repo_owner=None, owner_passwd=None):
		self.spice_repo = spice_repo
		self.repo_owner = repo_owner
		self.owner_passwd = owner_passwd
		#if owner is None:
			#print "What would you like to do?"
			#print ""
			#print "(g)et a listing of a repositories deploy keys"
			#print "(a)uthorize a deploy key with BitBucket"
			#print "(d)eauthorize a deploy key with BitBucket"
			#print ""
			#reply = raw_input("[")
			#if reply == "g":
				#pass
				##add definition for owner,owner_passwd,spice_repo cli
				##self.getKeys()
			#elif reply == "a":
				#pass
				##self.postKey(ssh_key,key_label)
			#elif reply == "d":
				#pass
				##add addition prompt for ssh keyid preceded by readout of key choices by 'label' and 'ak'
				##readout via 'for item in' sort of thing....?
				##self.delKey()
			#else:
				#return False

	def decode_list(self, jsonData):
		rv = []
		for item in jsonData:
			if isinstance(item, unicode):
				item = item.encode('utf-8')
			elif isinstance(item, list):
				item = self.decode_list(item)
			elif isinstance(item, dict):
				item = self.decode_dict(item)
			rv.append(item)
		return rv

	def decode_dict(self, jsonData):
		rv = {}
		for key, value in jsonData.iteritems():
			if isinstance(key, unicode):
			   key = key.encode('utf-8')
			if isinstance(value, unicode):
			   value = value.encode('utf-8')
			elif isinstance(value, list):
			   value = self.decode_list(value)
			elif isinstance(value, dict):
			   value = self.decode_dict(value)
			rv[key] = value
		return rv
	
	def getCred(self):
		credentials = b64encode("{0}:{1}".format(self.owner, self.owner_passwd).encode()).decode("ascii")
		return credentials
		
	def getURL(self):
		url = "https://api.bitbucket.org/1.0/repositories/%s/%s/deploy-keys" % (self.owner, self.spice_repo)
		return url
		
	def getHeaders(self):
		headers = {'Authorization': "Basic " + self.getCred()}
		return headers
	
	def getKeys(self):
		request = RESTRequest(url=self.getURL(), headers=self.getHeaders(), method="GET")
		connection = urlopen(request)
		content = connection.read()
		return json.loads(content, object_hook=self.decode_dict)
	
	def postKey(self,ssh_key,key_label):
		data = urlencode({"key": ssh_key, "label": key_label})
		request = RESTRequest(url=self.getURL(), headers=self.getHeaders(), data=data, method="POST")
		connection = urlopen(request)
		connection.read()
		
	def delKey(self,key_id):
		request = RESTRequest(url="%s/%s" % (self.getURL(), key_id), headers=self.getHeaders(), method="DELETE")
		connection = urlopen(request)
		connection.read()


#owner = raw_input("Owner account name for the Spices private repository? ")
## OUT: Owner account name for the Spices private repository?

#owner_passwd = raw_input("Password for %s? " % owner)
## OUT: Password for {owner}? 

#spice_repo = raw_input("Name of the Spices private repository? ")
## OUT: Name of the Spices private repository?

### SAMPLES

#mykey=DeployKey(owner=owner, owner_passwd=owner_passwd, spice_repo=spice_repo)

#mykey.postKey("ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTDh0DkpgC3EKsPp+BOb/dfHADAtu+8qkt5JquDBRD0Ry53MX9KYboNSyJV9XtBDtIome8Lga2dY3kzbXmtGzQvS0ciGF7HQPHVSKMl7ywG+5hNLN6gid8OJLg4RvkmGZIlLa0YL+yI4KimsOdglm+imUaDy/hv4uCY8q6bL6bsbSnkMzKds4/YxW7EPbFqTjF0Mgihrqgyu4yP6wmYrGY00cXFXMv8IJAb/nW2CalxNUvaPRGg6if40xxZwpuD0Nfjm95qq+oFUP6fEab5fdwa4pOYv2tIRaXhPO/27jeqlUSjAp/UXy5l3pKV3/TkjtN2Pyqs53QSOLgFrB0JqH7", "Beta2")

#mykeys=mykey.getKeys()
#print "Key dictionary: %s" % mykeys[0]
#print "Name: %s" % mykeys[0]['label']
#print "ID: %s" % mykeys[0]['pk']
#print "Key: %s" % mykeys[0]['key']
