# -*- coding: utf-8 -*-
import os
import djaboto

DJABOTO_COMMAND_DIR = os.path.join(
    os.path.dirname(djaboto.__file__), "management", "commands"
)


