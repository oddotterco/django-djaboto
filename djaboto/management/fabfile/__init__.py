__author__ = 'growlf'

from fabric import colors
from fabric.api import task, run, env, prompt
from djaboto.debian import install_system
import os, sys, boto


def gitReady(target_dir, git_url=None):
    """
    Clone or pull a repository as needed
    """
    import git

    # If the target already exists, attempt to refresh it
    if os.path.isdir(target_dir):
        try:
            repository = git.Repo(target_dir)
            o = repository.remotes.origin
            o.pull()
        except:
            print "Error occured when attempting to refresh the git repository at %s" % target_dir
            sys.exit(1)
    else:
        if git_url:
            repository = git.Repo.clone_from(git_url, target_dir)

@task
def check_os():
    """
    Check the local operating system
    """
    import platform
    is_64bits = sys.maxsize > 2**32

    print("-"*55)
    print platform.machine()
    if platform.system() == 'Linux':
        print platform.linux_distribution()[0]
    print platform.system()

    ###TODO: if Ubuntu, Linux - install all system modules
    ###TODO: if Ubuntu, Linux - link the correct PIL libs
    ###TODO: offer to reboot



@task
def check_pve():
    """
    Check the local python virtual environment for possible upgrades
    """
    import pip, xmlrpclib

    pypi = xmlrpclib.ServerProxy('http://pypi.python.org/pypi')
    for dist in pip.get_installed_distributions():
        available = pypi.package_releases(dist.project_name)

        if not available:
            # Try to capitalize pkg name
            available = pypi.package_releases(dist.project_name.capitalize())

        if available and (available[0] != dist.version):
            msg = '{} available'.format(available[0])
            pkg_info = '{dist.project_name} {dist.version}'.format(dist=dist)
            print '{pkg_info:40} {msg}'.format(pkg_info=pkg_info, msg=msg)

    ###TODO: check the local requirements.txt if it exists

@task
def check_debian():
    """
    Check and install any missing components for Django (Debian only)
    """
    install_system()


def makeppi():
    pass

def makedb():
    pass

def makedve():
    pass

def gitinit():
    pass

@task
def create_instance():
    """
    Create instance and git clone the site on the created instance
    Steps:
    - create an instance
    - system modules and updates - then restart
    - install and enable Apache config for site
    - install deployment key
    - makeppi
    - makedb (account, permissions)
    - makedve with project requirements.txt file
    - git clone the site
    - Cleanup and bump Apache
        rm -rf httpdocs/static/ && \
            ./manage.py clean_pyc && \
            ./manage.py syncdb --noinput && \
            ./manage.py migrate --noinput  &&  \
            ./manage.py collectstatic -l --noinput && \
            apache2ctl graceful
    """


    #run("mkdir -p ~/django")
    #run("cd ~/django")
    #run("mkdir bin/0a_makeppi python")

@task
def settings():
    """
    Display fabric and boto settings
    """

    print
    print("-"*55)
    for setting in env:
        if env[setting]:
            print("%55s : %-20s" % ( colors.yellow(setting), colors.cyan( env[setting])))
    print("-"*55)
    print("%55s : %-20s" % (colors.yellow('boto debug'), colors.cyan( boto.config.getboolean('Boto','debug') )))
    print("%55s : %-20s" % (colors.yellow('aws_access_key_id'), colors.cyan( boto.config.get('Credentials','aws_access_key_id') )))
    print("%55s : %-20s" % (colors.yellow('aws_secret_access_key'), colors.cyan( boto.config.get('Credentials','aws_secret_access_key') )))
    print

@task
def list_instances():
    """
    List all AWS instances for the configured ID
    """
    from djaboto.aws import get_awsInstanceList
    instance_list = get_awsInstanceList()
    if instance_list:
        for instance in instance_list:
            print('%s) %s (%s-%s)\t%s (%s/%s/%s)\t%s (%s)' % (
                colors.yellow(len(instance_list)),
                instance.id, instance.instance_type, instance.state,
                instance.image_id, instance.region.name, instance.architecture, instance.root_device_type,
                instance.public_dns_name, instance.ip_address,
                ))
    else:
        print("No instances found.")

    return instance_list

@task
def instance():
    """
    List information about the selected instance
    """

    from djaboto.aws import get_awsInstance
    instance = get_awsInstance()

    if instance:
        print("%55s: %s" % (colors.yellow('Instance'),instance.id))
        print("%55s: %s" % (colors.yellow('State'),instance.state))
        print("%55s: %s" % (colors.yellow('Architecture'),instance.architecture))
        print("%55s: %s" % (colors.yellow('Type'),instance.instance_type))
        print("%55s: %s" % (colors.yellow('Address'),instance.ip_address))
        print("%55s: %s" % (colors.yellow('Image'),instance.image_id))
        print("%s" % (instance.region))
    else:
        print("No instance is configured.")
        list_instances()
        prompt("Select an instance from above or '0' to create one:", instance_choice)
        print env[instance_choice]

